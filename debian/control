Source: superqt
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Stuart Prescott <stuart@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 pybuild-plugin-pyproject,
 python3-hatchling,
 python3-hatch-vcs,
Build-Depends-Indep:
# mkdocs <!nodoc>,          # building the docs needs newer mkdocs and mkdocs-macros
# mkdocs-material <!nodoc>,
# mkdocs-material-extensions <!nodoc>,
# mkdocstrings <!nodoc>,
# mkdocstrings-python-handlers <!nodoc>,
 python3-all,
 python3-pyconify <!nocheck>,
 python3-numpy <!nocheck>,
 python3-pint <!nocheck>,
 python3-pygments <!nocheck>,
 python3-qtpy <!nocheck>,
 python3-pyqt5 <!nocheck>,
 python3-pyqt6 <!nocheck>,
 python3-pyside6.qtgui <!nocheck>,
 python3-pyside6.qttest <!nocheck>,
 python3-pytest <!nocheck>,
 python3-pytestqt <!nocheck>,
 xauth <!nocheck>,
 xserver-xephyr <!nocheck>,
 xvfb <!nocheck>,
Standards-Version: 4.7.0
Homepage: https://github.com/pyapp-kit/superqt
Vcs-Git: https://salsa.debian.org/python-team/packages/superqt.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/superqt
Rules-Requires-Root: no

Package: python3-superqt
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-superqt-doc,
 python3-numpy,
 python3-pint,
 python3-pyconify,
Description: 'missing' widgets and components for PyQt/PySide
 The superqt project provides high-quality community-contributed Qt widgets
 and components for PyQt and PySide that are not provided in the
 native QtWidgets module. The module includes multihandle (range) sliders,
 comboboxes, and more.
 .
 The components are designed to be cross-platform and work with PyQt5, PyQt6,
 Pyside2, Pyside6.
 .
 This package contains the Python 3 module.

Package: python-superqt-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 ${misc:Depends},
Description: 'missing' widgets and components for PyQt/PySide - documentation
 The superqt project provides high-quality community-contributed Qt widgets
 and components for PyQt and PySide that are not provided in the
 native QtWidgets module. The module includes multihandle (range) sliders,
 comboboxes, and more.
 .
 The components are designed to be cross-platform and work with PyQt5, PyQt6,
 Pyside2, Pyside6.
 .
 This package contains the examples.
